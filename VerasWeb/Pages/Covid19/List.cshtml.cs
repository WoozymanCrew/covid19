using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using VerasWeb.Data;
using VerasWeb.Models;

namespace VerasWeb.Pages.Covid19
{
    public class ListModel : PageModel
    {
        private readonly IConfiguration _config;
        private readonly ICovid19Service _service;
        public string Name { get; set; }
        public IEnumerable<Covid19TestPerson> TestPersons { get; set; }
        [BindProperty(SupportsGet = true)]
        public string SearchTerm { get; set; }
        public ListModel(IConfiguration config, ICovid19Service service)
        {
            _config = config;
            _service = service;
        }
        public void OnGet()
        {
            Name = _config["MyName"];
            TestPersons = _service.GetSubjectByNameAsync(SearchTerm);
        }
    }
}
