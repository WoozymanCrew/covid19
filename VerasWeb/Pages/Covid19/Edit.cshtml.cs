using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using VerasWeb.Data;
using VerasWeb.Models;

namespace VerasWeb.Pages.Covid19
{
    public class Edit : PageModel
    {
        private readonly ICovid19Service _service;
        private readonly IHtmlHelper _htmlHelper;
        [BindProperty] public Covid19TestPerson TestPerson { get; set; }
        public Edit(ICovid19Service service, IHtmlHelper htmlHelper)
        {
            _service = service;
            _htmlHelper = htmlHelper;
        }

        public IActionResult OnGet(string subjectId)
        {
            if (!string.IsNullOrEmpty(subjectId))
            {
                TestPerson = _service.GetItemAsync(subjectId).Result;
            }
            else
            {
                TestPerson = new Covid19TestPerson();
            }

            if (TestPerson == null)
            {
                return RedirectToPage("./NotFound");
            }

            return Page();
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (!string.IsNullOrEmpty(TestPerson.Id))
            {
                _service.UpdateItemAsync(TestPerson.Id, TestPerson);
            }
            else
            {
                _service.AddItemAsync(TestPerson);
            }

            TempData["Message"] = "Test Subject Saved!";
            return Page();
        }
    }

    internal interface ICosmosDbService
    {
    }
}