﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using VerasWeb.Models;

namespace VerasWeb.Data
{
    public class Covid19Service : ICovid19Service
    {
        private readonly Container _container;
        public Covid19Service(
            CosmosClient dbClient,
            string databaseName,
            string containerName)
        {
            this._container = dbClient.GetContainer(databaseName, containerName);
        }
        public async Task AddItemAsync(Covid19TestPerson covid19TestPerson)
        {
            covid19TestPerson.Id = Guid.NewGuid().ToString();
            await this._container.CreateItemAsync<Covid19TestPerson>(covid19TestPerson,
                new PartitionKey(covid19TestPerson.Id));
        }
        public IEnumerable<Covid19TestPerson> GetSubjectByNameAsync(string name)
        {
            if (name != null)
            {
                return this.GetItemsAsync($"SELECT * FROM a where a.firstName = {name}").Result.ToList();
            }

            return this.GetItemsAsync("SELECT * FROM a").Result.ToList();
        }
        public async Task<Covid19TestPerson> GetItemAsync(string id)
        {
            try
            {
                ItemResponse<Covid19TestPerson> response =
                    await this._container.ReadItemAsync<Covid19TestPerson>(id, new PartitionKey(id));
                return response.Resource;
            }
            catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return null;
            }
        }
        public async Task<IEnumerable<Covid19TestPerson>> GetItemsAsync(string queryString)
        {
            var query = this._container.GetItemQueryIterator<Covid19TestPerson>(new QueryDefinition(queryString));
            var results = new List<Covid19TestPerson>();
            while (query.HasMoreResults)
            {
                var response = await query.ReadNextAsync();
                results.AddRange(response.ToList());
            }
            return results;
        }
        public async Task UpdateItemAsync(string id, Covid19TestPerson covid19TestSubject)
        {
            await this._container.UpsertItemAsync<Covid19TestPerson>(covid19TestSubject, new PartitionKey(id));
        }




    }
}
