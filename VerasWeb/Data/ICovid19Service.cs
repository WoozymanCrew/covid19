﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VerasWeb.Models;

namespace VerasWeb.Data
{
        public interface ICovid19Service
        {
            Task AddItemAsync(Covid19TestPerson covid19TestPerson);
            IEnumerable<Covid19TestPerson> GetSubjectByNameAsync(string name);
            Task<Covid19TestPerson> GetItemAsync(string id);
            Task<IEnumerable<Covid19TestPerson>> GetItemsAsync(string queryString);
            Task UpdateItemAsync(string id, Covid19TestPerson covid19TestSubject);
        }

}
