﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace VerasWeb.Models
{
    public class Covid19TestPerson
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [Required]
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }
        [Required]
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }
        [Required]
        [JsonProperty(PropertyName = "cprNumber")]
        [RegularExpression("\\d{6}-\\d{4}")]
        public string CprNumber { get; set; }

        [JsonProperty(PropertyName = "testResult")]
        public string TestResult { get; set; }

        [JsonProperty(PropertyName = "isTestResultComplete")]
        public bool Completed { get; set; }
    }
}
